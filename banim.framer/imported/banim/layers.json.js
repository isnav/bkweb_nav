window.__imported__ = window.__imported__ || {};
window.__imported__["banim/layers.json.js"] = [
	{
		"id": 71,
		"name": "bgmain",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 2000,
			"height": 4000
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 44,
				"name": "movie_play",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/movie_play.png",
					"frame": {
						"x": 1262,
						"y": 487,
						"width": 57,
						"height": 41
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1254339490"
			},
			{
				"id": 31,
				"name": "hire_button",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/hire_button.png",
					"frame": {
						"x": 1168,
						"y": 27,
						"width": 172,
						"height": 42
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1255024459"
			},
			{
				"id": 19,
				"name": "nav_expand",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/nav_expand.png",
					"frame": {
						"x": 111,
						"y": 130,
						"width": 145,
						"height": 40
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1255024491"
			},
			{
				"id": 17,
				"name": "nav_closed",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/nav_closed.png",
					"frame": {
						"x": 111,
						"y": 0,
						"width": 145,
						"height": 129
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1255024521"
			},
			{
				"id": 15,
				"name": "nav_open",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/nav_open.png",
					"frame": {
						"x": 111,
						"y": 0,
						"width": 145,
						"height": 320
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1255024553"
			},
			{
				"id": 5,
				"name": "bg",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/bg.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1442,
						"height": 3739
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1298219951"
			}
		],
		"modification": "1203245385"
	},
	{
		"id": 69,
		"name": "tf",
		"layerFrame": {
			"x": 2,
			"y": 1399,
			"width": 1443,
			"height": 700
		},
		"maskFrame": {
			"x": 2,
			"y": 1399,
			"width": 1443,
			"height": 700
		},
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 74,
				"name": "tftxt",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/tftxt.png",
					"frame": {
						"x": 705,
						"y": 1746,
						"width": 506,
						"height": 216
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1254101187"
			},
			{
				"id": 76,
				"name": "tfb",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 2000,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/tfb.png",
					"frame": {
						"x": 0,
						"y": 1396,
						"width": 1999,
						"height": 700
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1254190374"
			}
		],
		"modification": "872540305"
	}
]