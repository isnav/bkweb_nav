# This imports all the layers for "menu" into menuLayers
PSD = Framer.Importer.load "imported/banim"
# Make all the imported layers available on the root
for layerGroupName of PSD
  window[layerGroupName] = PSD[layerGroupName]

for layerGroupName of PSD
  PSD[layerGroupName].originalFrame = window[layerGroupName].frame
	
isitscrolled = false
# tfb.x = -300
# tftxt.x = 300
tf.x = 0
tf.y = 1396 
tfb.x = -200
tftxt.x = 905

bgmain.on Events.Click, ->
	if isitscrolled is false
		
		bgmain.animate
			properties: {y:-1200}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
		
		tf.animate
			properties: {y:196}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
			
		tftxt.animate
			properties: {x:705}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
			
		tfb.animate
			properties: {x:0}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
		
		isitscrolled = true
		return
	
	if isitscrolled is true
		
		bgmain.animate
			properties: {y:0}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
		
		tf.animate
			properties: {y:1396}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
			
		tftxt.animate
			properties: {x:905}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
			
		tfb.animate
			properties: {x:-200}
			curve: "bezier-curve"
			curveOptions: "ease-in-out"
			time:2
		
		isitscrolled = false
		return