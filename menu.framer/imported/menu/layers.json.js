window.__imported__ = window.__imported__ || {};
window.__imported__["menu/layers.json.js"] = [
	{
		"id": 5,
		"name": "bg",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/bg.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 1440,
				"height": 2000
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "726890213"
	},
	{
		"id": 15,
		"name": "nav_open",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/nav_open.png",
			"frame": {
				"x": 111,
				"y": 0,
				"width": 145,
				"height": 320
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1207192944"
	},
	{
		"id": 17,
		"name": "nav_closed",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/nav_closed.png",
			"frame": {
				"x": 111,
				"y": 0,
				"width": 145,
				"height": 129
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1207192942"
	},
	{
		"id": 19,
		"name": "nav_expand",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/nav_expand.png",
			"frame": {
				"x": 111,
				"y": 130,
				"width": 145,
				"height": 40
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "454583705"
	},
	{
		"id": 31,
		"name": "hire_button",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/hire_button.png",
			"frame": {
				"x": 1168,
				"y": 27,
				"width": 172,
				"height": 42
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1207192916"
	},
	{
		"id": 44,
		"name": "movie_play",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/movie_play.png",
			"frame": {
				"x": 1262,
				"y": 487,
				"width": 57,
				"height": 41
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1207192914"
	},
	{
		"id": 40,
		"name": "hire_curtain",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/hire_curtain.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 1440,
				"height": 2000
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1207192912"
	},
	{
		"id": 37,
		"name": "hire_popup",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/hire_popup.png",
			"frame": {
				"x": 273,
				"y": 144,
				"width": 896,
				"height": 539
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "454583704"
	},
	{
		"id": 59,
		"name": "movie_screen",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/movie_screen.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 1440,
				"height": 787
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1207192886"
	},
	{
		"id": 55,
		"name": "movie_scrubber",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1440,
			"height": 2000
		},
		"maskFrame": null,
		"image": {
			"path": "images/movie_scrubber.png",
			"frame": {
				"x": 0,
				"y": 787,
				"width": 1440,
				"height": 80
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1817737812"
	}
]