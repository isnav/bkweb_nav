# This imports all the layers for "menu" into menuLayers
PSD = Framer.Importer.load "imported/menu"
# Make all the imported layers available on the root
for layerGroupName of PSD
  window[layerGroupName] = PSD[layerGroupName]

for layerGroupName of PSD
  PSD[layerGroupName].originalFrame = window[layerGroupName].frame
	
menuShown = true
hirePopup = false
console.log "menuShown RESET = " + menuShown
bg.visible = true
nav_expand.opacity = 0
movie_scrubber.visible = false
movie_scrubber.y = 688
movie_screen.visible = false
hire_curtain.visible = false
hire_popup.visible = false
hire_curtain.opacity = 0
hire_popup.opacity = 0

bg.on Events.Click, ->
	if menuShown is true
		console.log "scroll up"
		
		bgAnimation = bg.animate 
			properties: {y: -800}
			curve: 'cubic-bezier'
			time: 0.5
			
		playAnimation = movie_play.animate 
			properties: {y: -313}
			curve: 'cubic-bezier'
			time: 0.5

		menuAnimation = nav_open.animate 
			properties: {opacity: 0, y: -400}
			curve: 'cubic-bezier'
			time: 0.6

		menuExpandAnimation = nav_expand.animate 
			properties: {opacity: 1}
			curve: 'cubic-bezier'
			time: 0.6
		
		menuShown = false
		console.log "menuShown = " + menuShown
		return
		
		
	if menuShown is false
		console.log "scroll down"
		
		bgAnimation = bg.animate 
			properties: {y: 0}
			curve: 'cubic-bezier'
			time: 0.5
			
		playAnimation = movie_play.animate 
			properties: {y:487}
			curve: 'cubic-bezier'
			time: 0.5

		menuAnimation = nav_open.animate 
			properties: {opacity: 1, y:0}
			curve: 'cubic-bezier'
			time: 0.6

		menuExpandAnimation = nav_expand.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.6
		
		menuShown = true
		console.log "menuShown = " + menuShown
		return
		
nav_expand.on Events.MouseOver, ->
	console.log "Mouseover, menuShown = " + menuShown
	if menuShown is false
		menuAnimation = nav_open.animate 
			properties: {opacity: 1, y:0}
			curve: 'cubic-bezier'
			time: 0.3

		menuExpandAnimation = nav_expand.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.3
		
		menuShown = true
		return
			
nav_expand.on Events.MouseOut, ->
	console.log "Mouse GONE, menuShown = " + menuShown
	if menuShown is true
		console.log menuShown
		menuAnimation = nav_open.animate 
			properties: {opacity: 0, y: -400}
			curve: 'cubic-bezier'
			time: 0.6
			delay: 2

		menuExpandAnimation = nav_expand.animate 
			properties: {opacity: 1}
			curve: 'cubic-bezier'
			time: 0.6
			delay: 2
		
		menuShown = false 
		return
			
hire_button.on Events.Click, ->
	if hirePopup is false
		hire_curtain.visible = true
		curtainAnimation = hire_curtain.animate 
			properties: {opacity: 1}
			curve: 'cubic-bezier'
			time: 0.4

		hire_popup.visible = true
		hire_popup.y = 100
		curtainAnimation = hire_popup.animate 
			properties: {opacity: 1, y: 200}
			curve: 'cubic-bezier'
			time: 0.4

		hirePopup = true
		return

	if hirePopup is true
		curtainAnimation = hire_curtain.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.4

		curtainAnimation = hire_popup.animate 
			properties: {opacity: 0, y: 100}
			curve: 'cubic-bezier'
			time: 0.4

		hirePopup = false
		setTimeout(hireHide, 300);
		return

hire_popup.on Events.Click, ->
	if hirePopup is true
		curtainAnimation = hire_curtain.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.4

		curtainAnimation = hire_popup.animate 
			properties: {opacity: 0, y: 100}
			curve: 'cubic-bezier'
			time: 0.4

		hirePopup = false
		setTimeout(hireHide, 300);
		return

hire_curtain.on Events.Click, ->
	if hirePopup is true
		curtainAnimation = hire_curtain.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.4

		curtainAnimation = hire_popup.animate 
			properties: {opacity: 0, y: 100}
			curve: 'cubic-bezier'
			time: 0.4

		hirePopup = false
		setTimeout(hireHide, 300)
		return

movie_play.on Events.Click, -> 
	if hirePopup is false
		console.log "clickity pop"
		hire_curtain.visible = true
		curtainAnimation = hire_curtain.animate 
			properties: {opacity: 1}
			curve: 'cubic-bezier'
			time: 0.8
		
		movie_screen.visible = true
		screenAnimation = movie_screen.animate 
			properties: {opacity: 1}
			curve: 'cubic-bezier'
			time: 0.6
		
		movie_scrubber.visible = true
		scrubberAnimation = movie_scrubber.animate 
			properties: {y:708}
			curve: 'spring-dho'
			curveOptions: 100,1,1,1,1
# 			stiffness, damping, mass, velocity, tolerance
			time: 0.6
		
		scrubberAnimation = movie_scrubber.animate 
			properties: {opacity: 1}
			curve: 'cubic-bezier'
			time: 0.6
			
		hirePopup = true
		return
	
movie_screen.on Events.Click, ->
	if hirePopup is true
		console.log "clickity WOW"
		curtainAnimation = hire_curtain.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.6 
		
		screenAnimation = movie_screen.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.6
		
		scrubberAnimation = movie_scrubber.animate 
			properties: {y:688}
			curve: 'cubic-bezier'
			time: 0.6
		
		scrubberAnimation = movie_scrubber.animate 
			properties: {opacity: 0}
			curve: 'cubic-bezier'
			time: 0.5
			
		hirePopup = false
		setTimeout(playerHide, 800)
		return 

hireHide = ->
	console.log "hire hiding"
	hire_curtain.visible = false
	hire_popup.visible = false
	return
	
playerHide = ->
	console.log "player hiding"
	hire_curtain.visible = false
	movie_scrubber.visible = false
	movie_screen.visible = false
	return